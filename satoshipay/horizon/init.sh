#!/bin/bash

docker-compose stop
sleep 2
docker-compose up -d --scale stellar-horizon=2
