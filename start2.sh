#!/bin/bash
set -x
export $(cat .env | xargs)

docker run -d \
    --env-file .env \
    -p "3000:8000" \
    -p "11626:11626" \
    -v "${VOLUME_DIR:-.}/stellar-data/horizon-core-data/horizon:/opt/stellar" \
    -v "${VOLUME_DIR:-.}/stellar-data/horizon-core-data/logs:/var/log/supervisor" \
    --name stellar stellar/quickstart
