#!/bin/bash
set -x
export $(cat .env | xargs)

rm -rf "$VOLUME_DIR/stellar-data/"
mkdir -p "$VOLUME_DIR/stellar-data"
docker-compose down -v
docker rm -f $(docker ps -a -q)
docker-compose up -d
docker-compose logs -f
