# stellar-node

Run Stellar core and horizon using `docker-compose`.

```bash
bash reset.sh
bash stop.sh
bash restart.sh
```

Obs:
* https://github.com/stellar/docker-stellar-core-horizon

* https://github.com/stellar/go/tree/master/services/horizon/docker

* Stellar-core docker image [satoshipay/stellar-core](https://github.com/satoshipay/docker-stellar-core)
* Horizon docker image based on [satoshipay/stellar-horizon](https://github.com/satoshipay/docker-stellar-horizon)