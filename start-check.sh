#!/bin/bash

function log() {
	echo "$(date): $@"
}

nohup ./check-services.sh 0<&- &> check-services.log &
